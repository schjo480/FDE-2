import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Column
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row


// (c) 2021 Thomas Neumann, Timo Kersten, Alexander Beischl, Maximilian Reif

object ReturnTrips {
  val earthRadius = 6371000.0



  def compute(trips : Dataset[Row], dist : Double, spark : SparkSession) : Dataset[Row] = {

    import spark.implicits._

    //ToDo: Add your implementation here

    // Haversine distance.
    val makeDistExpr = (lat1 : Column, lon1 : Column, lat2 : Column, lon2 : Column) => {
      val dLat = toRadians(abs(lat2 - lat1))
      val dLon = toRadians(abs(lon2 - lon1))
      val hav = pow(sin(dLat*0.5),2) + pow(sin(dLon*0.5),2) * cos(toRadians(lat1)) * cos(toRadians(lat2))
      abs(lit(earthRadius * 2) * asin(sqrt(hav)))
    }


    var a = trips.select($"pickup_longitude", $"pickup_latitude", $"tpep_pickup_datetime".as("pickup_datetime"),
      $"dropoff_longitude", $"dropoff_latitude", $"tpep_dropoff_datetime".as("dropoff_datetime")).cache()


    // Convert times to UNIX
    // Create buckets for dropoff and pickup time ==> 8*60*60 is enough
    // Create buckets for pickup and dropoff latitude (0.002°latitude ~ 222.4m)
    a = a.withColumn("pickup_datetime", unix_timestamp($"pickup_datetime")).withColumn(
      "dropoff_datetime", unix_timestamp($"dropoff_datetime")).withColumn(
      "dropoff_time_bucket", floor($"dropoff_datetime"/(9*60*60))).withColumn(
      "pickup_time_bucket", floor($"pickup_datetime"/(9*60*60))).withColumn("pickup_latitude_bucket", floor($"pickup_latitude"/0.002)).withColumn(
      "dropoff_latitude_bucket", floor($"dropoff_latitude"/0.002))


    // Copy dataframe
    var b = a.withColumn("pickup_time_bucket", explode(array($"pickup_time_bucket" - 1, $"pickup_time_bucket"))).withColumn("pickup_latitude_bucket", explode(array($"pickup_latitude_bucket" - 1,
      $"pickup_latitude_bucket", $"pickup_latitude_bucket" + 1)))
    a = a.withColumn("pickup_latitude_bucket", explode(array($"pickup_latitude_bucket" - 1,
      $"pickup_latitude_bucket", $"pickup_latitude_bucket" + 1)))

    // Conditions:
    // trips.dropoff_time_bucket = b.pickup_time_bucket
    // trips.pickup_latitude_bucket = b.dropoff_latitude_bucket

    // Create buckets for dropoff and pickup latitude
    /*b.withColumn("pickup_latitude_bucket", floor($"pickup_latitude"/1))
    b.withColumn("pickup_latitude_bucket", explode(array($"pickup_latitude_bucket" - 1,
     $"pickup_latitude_bucket", $"pickup_latitude_bucket" + 1))).cache()
    a.withColumn("dropoff_latitude_bucket", floor($"dropoff_latitude"/1)).cache()*/


    a.createOrReplaceTempView("a")
    b.createOrReplaceTempView("b")


    var j = spark.sql(
      "SELECT /*+ MERGEJOIN(a, b) */ a.dropoff_datetime adropdate, b.pickup_datetime bpickdate, a.pickup_latitude apicklat, a.pickup_longitude apicklong, a.dropoff_latitude adroplat, a.dropoff_longitude adroplong, b.pickup_latitude bpicklat, b.pickup_longitude bpicklong, b.dropoff_latitude bdroplat, b.dropoff_longitude bdroplong FROM a a, b b WHERE a.dropoff_time_bucket == b.pickup_time_bucket AND a.pickup_latitude_bucket == b.dropoff_latitude_bucket AND a.dropoff_latitude_bucket == b.pickup_latitude_bucket")
    // AND b.pickup_latitude_bucket == a.dropoff_latitude_bucket")


    j.filter(($"adropdate" < $"bpickdate") &&
      ($"adropdate" + 8 * 60 * 60 > $"bpickdate") &&
      (makeDistExpr($"apicklat", $"apicklong",
        $"bdroplat", $"bdroplong") < lit(dist)) &&
      (makeDistExpr($"adroplat", $"adroplong",
        $"bpicklat", $"bpicklong") < lit(dist)))

  }
}


// Create buckets for pickup and dropoff time (day-1, day, day+1)
/*a = a.withColumn("pickup_day_bucket", floor($"pickup_datetime"/(24*60*60)))
var b = a.withColumn("dropoff_day_bucket", floor($"dropoff_datetime"/(24*60*60)))
b = b.withColumn("dropoff_day_bucket", explode(array($"dropoff_day_bucket" - 1, $"dropoff_day_bucket", $"dropoff_day_bucket" + 1)))*/

// Create buckets for pickup and dropoff longitude (-0.2°, 0, +0.2°)
/*a = a.withColumn("pickup_longitude_bucket", floor($"pickup_longitude"/0.5))
b = b.withColumn("dropoff_longitude_bucket", floor($"dropoff_longitude"/0.5))
b = b.withColumn("dropoff_longitude_bucket", explode(array($"dropoff_longitude_bucket" - 1,
 $"dropoff_longitude_bucket", $"dropoff_longitude_bucket" + 1)))*/

// Create buckets for dropoff and pickup latitude
/*a = a.withColumn("dropoff_latitude_bucket", floor($"dropoff_latitude"/1)).cache()
b = b.withColumn("pickup_latitude_bucket", floor($"pickup_latitude"/1))
b = b.withColumn("pickup_latitude_bucket", explode(array($"pickup_latitude_bucket" - 1,
 $"pickup_latitude_bucket", $"pickup_latitude_bucket" + 1))).cache()*/

// Create buckets for dropoff and pickup longitude
/*a = a.withColumn("dropoff_longitude_bucket", floor($"dropoff_longitude"/0.1)).cache()
b = b.withColumn("pickup_longitude_bucket", floor($"pickup_longitude"/0.1))
b = b.withColumn("pickup_longitude_bucket", explode(array($"pickup_longitude_bucket" - 1,
 $"pickup_longitude_bucket", $"pickup_longitude_bucket" + 1))).cache()*/


/*
// Join
val j = a.as("a").join(b.as("b"),
 ($"a.dropoff_day_bucket" === $"b.pickup_day_bucket")
 //($"a.pickup_day_bucket" === $"b.dropoff_day_bucket") &&
 //&& ($"a.pickup_latitude_bucket" === $"b.dropoff_latitude_bucket")
 //&& ($"a.dropoff_latitude_bucket" === $"b.pickup_latitude_bucket")
)
j.filter(
   ($"a.dropoff_datetime" < $"b.pickup_datetime") &&
   ($"a.dropoff_datetime" + 8 * 60 * 60 > $"b.pickup_datetime") &&
   (makeDistExpr($"a.pickup_latitude", $"a.pickup_longitude",
     $"b.dropoff_latitude", $"b.dropoff_longitude") < lit(dist)) &&
   (makeDistExpr($"a.dropoff_latitude", $"a.dropoff_longitude",
     $"b.pickup_latitude", $"b.pickup_longitude") < lit(dist)))
 //($"a.pickup_longitude_bucket" === $"b.dropoff_longitude_bucket") &&
 //($"a.dropoff_latitude_bucket" === $"b.pickup_latitude_bucket") &&
 //($"a.dropoff_longitude_bucket" === $"b.pickup_longitude_bucket") &&*/

/*
spark.sql('''
  select /*+ MERGEJOIN(a, b) */ *
  from a, b
  where a.time_bucket = b.time_bucket and a.pickup_latitude_bucket = b.pickup_latitude_bucket
  ''').filter(($"a.dropoff_datetime" < $"b.pickup_datetime") &&
    ($"a.dropoff_datetime" + 8 * 60 * 60 > $"b.pickup_datetime") &&
    (makeDistExpr($"a.pickup_latitude", $"a.pickup_longitude",
      $"b.dropoff_latitude", $"b.dropoff_longitude") < lit(dist)) &&
    (makeDistExpr($"a.dropoff_latitude", $"a.dropoff_longitude",
      $"b.pickup_latitude", $"b.pickup_longitude") < lit(dist)))*/
